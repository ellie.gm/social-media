from django.contrib import admin
from .models import Message, Discussion

# Register your models here.
admin.site.register(Message)
admin.site.register(Discussion)
