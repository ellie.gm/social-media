# Generated by Django 2.1.2 on 2018-12-06 08:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('discussions', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='discussion',
            old_name='user',
            new_name='members',
        ),
        migrations.RenameField(
            model_name='message',
            old_name='user',
            new_name='owner',
        ),
        migrations.AddField(
            model_name='discussion',
            name='timestamp',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
