from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.generic import View
from .form import MessageForm
from .models import Message, Discussion
from django.urls import reverse


def discussion_view(request):
    if request.user.is_authenticated:
        message_form = MessageForm
        template_name = 'discussions/discussions.html'
        form = message_form(None)
        return render(request, template_name, {'form': form})
    else:
        return HttpResponseRedirect(reverse('members:login'))


class DiscussionHandler(View):

    def get(self, request, discussion_id):
        if request.user.is_authenticated:
            template_name = 'discussions/discussions.html'
            discussion = Discussion.get_discussion_by_id(discussion_id)
            context = {}
            if discussion:
                context['discussion'] = discussion.to_json()
                context['discussion_owner_username'] = discussion.owner.get_username()
            return render(request, template_name, context)
        else:
            return HttpResponseRedirect(reverse('members:login'))

    def post(self, request, discussion_id):
        action = request.POST.get('action', '')
        if action == 'create':
            username = request.POST.get('username', '')
            title = request.POST.get('title', '')
            Discussion.create_discussion(title=title, owner_username=username)
            return JsonResponse({'message': 'ok'})
        if action == 'list_member_discussions':
            username = request.POST.get('username', '')
            discussion_list = Discussion.get_member_discussions_json(username)
            reuslt = {"data": discussion_list}
            return JsonResponse(reuslt)
        if action == 'list_discussions':
            discussion_list = Discussion.get_discussions_json()
            result = {'data': discussion_list}
            return JsonResponse(result)
        if action == 'join_discussion':
            discussion_id = request.POST.get('discussion_id', '')
            username = request.POST.get('username', '')
            Discussion.add_member_to_discussion(discussion_id=discussion_id, username=username)
            return JsonResponse({'message': 'ok'})
        if action == 'leave_discussion':
            discussion_id = request.POST.get('discussion_id', '')
            username = request.POST.get('username', '')
            Discussion.remove_member_from_discussion(discussion_id=discussion_id, username=username)
            return JsonResponse({'message': 'ok'})


class MessageHandler(View):

    def post(self, request):
        action = request.POST.get('action', '')
        if action == 'list_discussion_messages':
            discussion_id = request.POST.get('discussion_id', '')
            messages = Message.get_discussion_messages_json(discussion_id)
            result = {'data': messages}
            return JsonResponse(result)

        if action == 'delete':
            message_id = request.POST.get('message_id', '')
            Message.delete_message(message_id)
            result = {'message': 'ok'}
            return JsonResponse(result)

        if action == 'create':
            message = Message.create_message(owner_username=request.POST.get('username', ''),
                                             discussion_id=request.POST.get('discussion_id', ''),
                                             content=request.POST.get('content', ''))
            result = {'message': 'ok', 'data': message}
            return JsonResponse(result)
