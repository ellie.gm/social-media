from .models import Message
from django import forms


class MessageForm(forms.ModelForm):

    message = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Message
        fields = ['message']



