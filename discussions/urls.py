from django.urls import path
from discussions.views import discussion_view, DiscussionHandler, MessageHandler

app_name = 'discussions'

urlpatterns = [
path('message/', MessageHandler.as_view(), name='message'),
    path('discussion/<discussion_id>', DiscussionHandler.as_view(), name='discussion'),
    path('', discussion_view, name='discussion_view'),
]
