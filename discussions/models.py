from members.models import Member
from django.db import models
from django.utils import timezone


class Discussion(models.Model):
    title = models.TextField(max_length=500)
    timestamp = models.DateTimeField(blank=True, null=True)
    members = models.ManyToManyField(Member)
    owner = models.ForeignKey(Member, on_delete=models.CASCADE, related_name='owner')

    def __str__(self):
        return "title: %s, timestamp: %s" % (
            self.title, self.timestamp,)

    def to_json(self):
        return {'id': self.id, 'timestamp': self.timestamp, 'title': self.title, 'owner':self.owner.get_username()}

    def add_member(self, username):
        member = Member.find_member_by_username(username)
        self.members.add(member)
        self.save()

    def remove_member(self, username):
        member = Member.find_member_by_username(username)
        self.members.remove(member)
        self.save()
        if len(self.members.all()) == 0:
            self.delete()

    @staticmethod
    def create_discussion(title, owner_username):
        member = Member.find_member_by_username(owner_username)
        discussion = Discussion(title=title, timestamp=timezone.now(), owner=member)
        discussion.save()
        discussion.members.add(member)
        discussion.save()

    @staticmethod
    def get_member_discussions(username):
        return Discussion.objects.filter(members__user__username=username)

    @staticmethod
    def get_member_discussions_json(username):
        discussions = Discussion.get_member_discussions(username)
        result = []
        for discussion in discussions.all():
            result.append(discussion.to_json())
        return result

    @staticmethod
    def get_discussions():
        return Discussion.objects.all()

    @staticmethod
    def get_discussions_json():
        discussion_list = Discussion.get_discussions()
        result = []
        for discussion in discussion_list.all():
            result.append(discussion.to_json())
        return result

    @staticmethod
    def add_member_to_discussion(discussion_id, username):
        Discussion.objects.get(pk=discussion_id).add_member(username)

    @staticmethod
    def remove_member_from_discussion(discussion_id, username):
        Discussion.objects.get(pk=discussion_id).remove_member(username)

    @staticmethod
    def get_discussion_by_id(discussion_id):
        return Discussion.objects.get(pk=discussion_id)


class Message(models.Model):
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey(Member, on_delete=models.CASCADE)
    discussion = models.ForeignKey(Discussion, on_delete=models.CASCADE)

    def __str__(self):
        return "owner: %s, timestamp: %s, discussion: %s, content: %s" % (
            self.owner.get_username(), self.timestamp, self.discussion.id, self.content)

    def to_json(self):
        return {'id': self.id, 'content': self.content, 'timestamp': self.timestamp,
                'discussion_id': self.discussion.id,
                'owner_id': self.owner.id, 'owner_username': self.owner.get_username(),
                'owner_name': self.owner.get_full_name()}

    @staticmethod
    def create_message(content, discussion_id, owner_username):
        owner = Member.find_member_by_username(owner_username)
        discussion = Discussion.get_discussion_by_id(discussion_id)
        message = Message(content=content, owner=owner, discussion=discussion, timestamp=timezone.now())
        message.save()
        return message.to_json()

    @staticmethod
    def delete_message(message_id):
        Message.objects.get(pk=message_id).delete()

    @staticmethod
    def get_discussion_messages(discussion_id):
        return Message.objects.filter(discussion=discussion_id).order_by('timestamp')

    @staticmethod
    def get_discussion_messages_json(discussion_id):
        messages = Message.get_discussion_messages(discussion_id)
        result = []
        for message in messages.all():
            result.append(message.to_json())
        return result
