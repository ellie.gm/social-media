# Initial project plan document

## Course project group information

Names: Niloufar Valinejad(267646), Zahra Golmohammadi(272492), Hossein Malekzadeh(281662)

Emails: niloufar.valineja@student.tut.fi, zahra.golmohammadi@student.tut.fi, hossein.malekzadeh@student.tut.fi

Group name: WIR

GitLab repo URL: https://course-gitlab.tut.fi/bwa-2018/bwa-group009

Heroku application URL
https://nelin.herokuapp.com/

Heroku deployment URL:
https://git.heroku.com/nelin.git


## Must have features

|Order| Feature  | Initial Deadline | Status | Finished on |
| -------- | -------- | -------- | -------- | -------- |
|1| User Profile  | 4-11-2018|Done|8-12-2018|
|2| User Registration | 6-11-2018 |Done|8-12-2018|
|3| user login/logout | 8-11-2018 |Done|8-12-2018|
|4| delete user profile | 8-11-2018 |Done|8-12-2018|
|5| user profile modification| 8-11-2018 |done|8-12-2018|
|6| Display user profile (public/private)| 8-11-2018 |Done|8-12-2018|
|7| Sending a friend request|15-11-2018|Done|8-12-2018|
|8| managing a friend request (view list, delete, accept, reject)|15-11-2018|Done|8-12-2018|
|9| managing friendship (view list, delete)|15-11-2018|Done|8-12-2018|
|10| managing discussions (view list, create, delete, join, leave)|22-11-2018|Done|8-12-2018|
|12| managing message in discussion (send and delete)|22-11-2018|Done|8-12-2018|
|15| admin user | 8-11-2018 |Done|8-12-2018|



## Planned +2 features

|Order| Star | Feature  | Deadline | Status |Finished on |
| -----| -------- | -------- | -------- |-------- |-------- |
|0| * | using separate django apps | 04-12-2018 | Done ||
|0| ** | Using Bootstrap for mobile friendliness | 04-12-2018 | Done ||
|0| ** | Use PostgreSQL as database | 04-12-2018 | Done ||
|0|***| REST API | 04-12-2018 | Done| |
|16| * | Status messages on the users’ profile pages | 04-12-2018 | Done |8-12-2018|

order 0 means it was implemented from the beginning. 


## Pages and navigation
![](documentation_related_files/pages_and_navigations.jpg)

This application is mage of five pages
    
    - Login : from which user can login or select to registr as a new user
    - New User Registration :in which a new user register and will be redirected to his/her profile page
    - Profile: In this page, user can see his/her personal infroamtion, select to edit them, 
      delete the profile or post a status message, also from this page user can navigate to friends section or discussions.
      and in case a friend request gets accepted or rejected, user will get a message in this page
    - Edit Personal Information: this is the view in which user can edit his/her personal information     
    - Friends : in this page user can send a friend request, see list of send reqeust and received reqeust, user can delete sent request and accept or reject received reqeust, 
    aslo user can see a list of his/her friends in this page, he can navigate to the friends profile page by clicking on theri name or delete the firendship.
    - Discussions: in this page, user can initiate a new disucssion, join and leave discussions and send message to the joind discussions. it should be noted if there no users in a discussion it will be removed.
    
## Technological considerations

### Django apps in your Django project
project consists of three applications

    - Members: this application implements the functionality of user profile, user status message and friendships. 
    - Discussions:  this application implements the functionality of discussions

### Needed Django models and their attributes


    Member
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=20)
    address = models.CharField(max_length=50)
    friends = models.ManyToManyField('self')
    
    FriendRequest
    REQUEST_STATUS = (('Accepted', 'Accepted'), ('Rejected', 'Rejected'), ('Open', 'Open'))
    sender = models.ForeignKey(Member, on_delete=models.CASCADE, related_name='sender')
    receiver = models.ForeignKey(Member, on_delete=models.CASCADE, related_name='receiver')
    created_time = models.DateTimeField()
    closed_time = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=15, choices=REQUEST_STATUS)
    informed = models.BooleanField(null=True, blank=True, default=False)
    
    
    Status
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now=True)
    status_owner = models.ForeignKey(Member, on_delete=models.CASCADE, related_name='status_owner')
    update_timestamp = models.DateTimeField(blank=True, null=True)
    
    Comment
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey(Member, on_delete=models.CASCADE)
    status = models.ForeignKey(Status, on_delete=models.CASCADE)
    
    
    Discussion
    title = models.TextField(max_length=500)
    timestamp = models.DateTimeField(blank=True, null=True)
    members = models.ManyToManyField(Member)
    owner = models.ForeignKey(Member, on_delete=models.CASCADE, related_name='owner')
    
    Message
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey(Member, on_delete=models.CASCADE)
    discussion = models.ForeignKey(Discussion, on_delete=models.CASCADE)

### URIs
| URL| Page|
| -----| -------- | 
|| landing page, redirects to login page|
|'admin/'| admin panel|
|err404| error 404 page|
|err500| error 500 page|
|members/login/|login page|
|members/signup/|new user signup page|
|members/info/<username>|personal information edit page|
|members/profile/<username>|user profile view|
|members/friendsview/<username>|user friends page view|
|members/friends/<username>|url to mange friends, all the ajax calls regarding friends are made to this url (in rest format)|
|members/friendrequest/<username>| url to manage friend requests, all the ajax calls regarding friend request are made to this url (in rest format)|
|members/logout|a call to this url logs out current user|
|members/status|url to manage user status messages, all the ajax messages regarding user status messages are made to this url (in rest format)|
|members/comment|url to manage comments on the user status message, all the ajax messages regarding comments are made to this url (in rest format)|
|members/delete| a call to this url will delete current user|
|discussions/message/|url to manage messages in the discussions, all the ajax calls regarding messages are made to this rul ( in rest format)|
|discussions/discussion/<discussion_id>'| this is the specific for each url a GET call to this will get discussion, which, is specified by the discussion_id, also this url is to manages the disucssions, all the ajax calls regarding discussions are made to this url (in rest format)  |
|discussions/|discussions page|
    
    

### Needed Django views and templates

|Application|View|Template|Information|
| -----| -------- | -------- | -------- |
|members|profile| members/profile.html| this template is composed of members/user_info.html which display user information and "members/status.html" which handles the whole user status message functionality
|members|delete_user|redirect to login|
|members|friends_view| members/friends.html
|members|logout|redirect to login|
|members|Friends|return json|
|members|FriendRequestHandler|return json|
|members|SignUp| members/registration_form.html|
|members|UserInformation|  members/member_info_form.html|
|members|SignIn| 'members/login_form.html'|
|members|StatusHandler|return json|
|members|CommentHandler|return json|

### Heroku deployment

URL: https://git.heroku.com/nelin.git

all team members installed and were given required permission to Heroku account, the main person responsible for maintenance and deployment were Hossein Malekzadeh

## Testing

Each person test its own code and in code reviews coders check their team mates codes.

## Deployment to Heroku

    $ git push nelin master
    
    We connected out development environments to the postgresql in the Heroku and all the migrations are already done, so this is enough, just to push the code to Heroku repository.
    We noticed this is not a good practice to have the same database for developement and production, however we diceded to the purpose of this course it makes it easier and faster.

## MISC
### AJAX
This application relies heavily on ajax, in most views with the first call, structure of the page is rendered on server, 
after that main portion of information is retrieved via ajax calls and related view element are rendered on the client side.
However it's just dynamic that's rendered in the client side, for example, in the friends page, an empty page, contains 
the form for creating new request, container for sent request, container for received request, container for list of friends, and 
these containers are populated on the client side.

### Bootstrap
Whole styling of application has been done with bootstrap with some custom css, in addition with help of bootstrap grid we have tried to achieve responsive views, however on some mobile phones the responsiveness is not working. 

## current users

### admin users
|username|password|
| -----| -------- |
|admin|salam123|
|admin2|salam123|
### normal users
|username|password|
| -----| -------- |
|user1|123|
|user2|123|
|user3|123|
|user4|123|
|user5|123|


## License: MIT


