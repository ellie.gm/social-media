from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render, redirect


def index(request):
    return HttpResponseRedirect(reverse('members:login'))


def not_found(request):
    return render(request, "not_found.html")

def error_500(request):
    return render(request, "error_500.html")
