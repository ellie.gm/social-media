from django.contrib import admin
from .models import Member, Status, FriendRequest,Comment

# Register your models here.
admin.site.register(Member)
admin.site.register(Status)
admin.site.register(FriendRequest)
admin.site.register(Comment)
