# Generated by Django 2.1.2 on 2018-12-03 19:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0009_auto_20181201_0811'),
    ]

    operations = [
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content', models.TextField()),
                ('timestamp', models.DateTimeField(auto_now=True)),
                ('status_owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='status_owner', to='members.Member')),
            ],
        ),
    ]
