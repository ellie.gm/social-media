from django.contrib.auth.models import User
from django.db import models
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist


class Member(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=20)
    address = models.CharField(max_length=50)
    friends = models.ManyToManyField('self')

    def __str__(self):
        return "first_name: %s, last_name: %s, username: %s, email: %s, phone_number: %s, address: %s" % (
            self.user.first_name, self.user.last_name, self.user.username, self.user.email, self.phone_number,
            self.address)

    def to_json(self, show_private=False):
        member = {"id": self.id, "first_name": self.user.first_name, "last_name": self.user.last_name,
                  "username": self.user.username, "email": self.user.email}

        if show_private:
            member["phone_number"] = self.phone_number
            member["address"] = self.address
        return member

    def create_member(self, first_name, last_name, username, password, email, phone_number, address):
        user = User(first_name=first_name, last_name=last_name, username=username, email=email)
        user.set_password(password)
        user.save()
        self.user = user
        self.phone_number = phone_number
        self.address = address
        self.save()

    def is_fried_with(self, username):
        member = self.friends.get(user__username=username)
        if member is not None:
            return True
        else:
            return False

    def get_username(self):
        return self.user.username

    def get_full_name(self):
        return self.user.first_name + ' ' + self.user.last_name

    def add_friend(self, friend):
        try:
            self.friends.add(friend)
            return True
        except:
            return False

    @staticmethod
    def delete_member(username):
        member = Member.find_member_by_username(username)
        user = User.objects.get(pk=member.user.id)
        user.delete()

    @staticmethod
    def update_information(username, first_name, last_name, phone_number, address):
        member = Member.find_member_by_username(username)
        if member is not None:
            member.user.first_name = first_name
            member.user.last_name = last_name
            member.user.save()
            member.phone_number = phone_number
            member.address = address
            member.save()
            return True
        return False

    @staticmethod
    def remove_friend(username, friend_username):
        user = Member.find_member_by_username(username)
        friend = Member.find_member_by_username(friend_username)
        sent_requests = FriendRequest.objects.filter(sender=user.id, receiver=friend.id)
        for request in sent_requests.all():
            request.delete()
        received_requests = FriendRequest.objects.filter(sender=friend.id, receiver=user.id)
        for request in received_requests.all():
            request.delete()
        user.friends.remove(friend)

    @staticmethod
    def get_member_friends_by_username(username):
        return Member.find_member_by_username(username).friends

    @staticmethod
    def get_member_friends_by_username_json(username):
        friends = Member.get_member_friends_by_username(username)
        friends_json = []
        for friend in friends.all():
            friends_json.append(friend.to_json())
        return friends_json

    @staticmethod
    def find_member_by_username(username):
        member = Member.objects.get(user__username=username)
        return member

    @staticmethod
    def find_member_by_id(member_id):
        return Member.objects.get(pk=member_id)

    @staticmethod
    def login(username, password, request):
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return user
        else:
            return None

    @staticmethod
    def logout(request):
        logout(request)


class FriendRequest(models.Model):
    REQUEST_STATUS = (('Accepted', 'Accepted'), ('Rejected', 'Rejected'), ('Open', 'Open'))
    sender = models.ForeignKey(Member, on_delete=models.CASCADE, related_name='sender')
    receiver = models.ForeignKey(Member, on_delete=models.CASCADE, related_name='receiver')
    created_time = models.DateTimeField()
    closed_time = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=15, choices=REQUEST_STATUS)
    informed = models.BooleanField(null=True, blank=True, default=False)

    def __str__(self):
        return "sender: %s, receiver: %s, created_time: %s, closed_time: %s, status: %s" % (
            self.sender, self.receiver, self.created_time, self.closed_time, self.status)

    def to_json(self):
        return {"id": self.id,
                "sender": self.sender.get_username(),
                'receiver': self.receiver.get_username(),
                'created_time': self.created_time, 'closed_time': self.closed_time, 'status': self.status}

    @staticmethod
    def create_friend_request(sender_username, receiver_username):
        sender = Member.find_member_by_username(sender_username)
        receiver = Member.find_member_by_username(receiver_username)
        created_time = timezone.now()
        try:
            friend_request = FriendRequest.objects.get(sender=sender.id, receiver=receiver.id)
            if friend_request.status != 'Open':
                friend_request.status = 'Open'
                friend_request.created_time = timezone.now()
                friend_request.informed = False
                friend_request.save()
        except ObjectDoesNotExist:
            friend_request = FriendRequest(sender=sender, receiver=receiver, created_time=created_time, status='Open')
            friend_request.save()

    @staticmethod
    def get_friend_requests(username):
        member = Member.find_member_by_username(username)
        return FriendRequest.objects.filter(Q(sender=member.id) | Q(receiver=member.id))

    @staticmethod
    def get_friend_requests_json(sender_username):
        requests = FriendRequest.get_friend_requests(sender_username)
        requests_json = []
        for request in requests:
            requests_json.append(request.to_json())
        return requests_json

    @staticmethod
    def get_closed_friend_requests_json(sender_username):
        requests = FriendRequest.get_friend_requests(sender_username)
        accepted = []
        rejected = []
        result = {}
        for request in requests.all():
            if request.status != 'Open' and request.informed is False:
                if request.status == 'Accepted':
                    accepted.append(request.to_json())
                if request.status == 'Rejected':
                    rejected.append(request.to_json())
                request.informed = True
                request.save()
        result['accepted'] = accepted
        result['rejected'] = rejected
        return result

    @staticmethod
    def delete_friend_request(request_id):
        FriendRequest.objects.get(pk=request_id).delete()

    @staticmethod
    def accept_friend_request(request_id):
        request = FriendRequest.objects.get(pk=request_id)
        request.status = 'Accepted'
        request.closed_time = timezone.now()
        request.save()
        request.sender.add_friend(request.receiver)

    @staticmethod
    def reject_friend_request(request_id):
        request = FriendRequest.objects.get(pk=request_id)
        request.status = 'Rejected'
        request.closed_time = timezone.now()
        request.save()

    class Meta:
        unique_together = ("sender", "receiver")


class Status(models.Model):
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now=True)
    status_owner = models.ForeignKey(Member, on_delete=models.CASCADE, related_name='status_owner')
    update_timestamp = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.content

    def to_json(self, current_username):
        comments = Comment.get_status_comments_json(self.id)
        can_comment = False
        if current_username:
            if self.status_owner.get_username() == current_username:
                can_comment = True
            elif self.status_owner.is_fried_with(current_username):
                can_comment = True

        return {'id': self.id, 'content': self.content, 'timestamp': self.timestamp, 'can_comment': can_comment,
                'update_timestamp': self.update_timestamp, 'owner_id': self.status_owner.id,
                'owner_username': self.status_owner.get_username(),
                'owner_full_name': self.status_owner.get_full_name(),
                'comments': comments}

    @staticmethod
    def create_status(content, owner_username):
        member = Member.find_member_by_username(owner_username)
        status = Status(content=content, timestamp=timezone.now(), status_owner=member)
        status.save()

    @staticmethod
    def update_status(status_id, content):
        status = Status.objects.get(pk=status_id)
        status.content = content
        status.update_timestamp = timezone.now()
        status.save()

    @staticmethod
    def delete_status(status_id):
        Status.objects.get(pk=status_id).delete()

    @staticmethod
    def get_member_status_list(member_username):
        member = Member.find_member_by_username(member_username)
        return Status.objects.filter(status_owner=member).order_by('-timestamp')

    @staticmethod
    def get_member_status_list_json(member_username, current_username):
        status_list = Status.get_member_status_list(member_username)
        result = []
        for status in status_list.all():
            result.append(status.to_json(current_username))
        return result

    @staticmethod
    def update_status_content(status_id, content):
        status = Status.objects.get(pk=status_id)
        status.content = content
        status.save()


class Comment(models.Model):
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey(Member, on_delete=models.CASCADE)
    status = models.ForeignKey(Status, on_delete=models.CASCADE)

    def __str__(self):
        return "owner: %s, timestamp: %s, status: %s, content: %s" % (
            self.owner.get_username(), self.timestamp, self.status.id, self.content)

    def to_json(self):
        return {'id': self.id, 'content': self.content, 'timestamp': self.timestamp, 'status_id': self.status.id,
                'owner_id': self.owner.id, 'owner_username': self.owner.get_username(),
                'owner_name': self.owner.get_full_name()}

    @staticmethod
    def create_comment(owner_username, status_id, content):
        status = Status.objects.get(pk=status_id)
        owner = Member.find_member_by_username(owner_username)
        comment = Comment(content=content, timestamp=timezone.now(), owner=owner, status=status)
        comment.save()

    @staticmethod
    def delete_comment(comment_id):
        Comment.objects.get(comment_id).delete()

    @staticmethod
    def update_comment(comment_id, content):
        comment = Comment.objects.get(comment_id)
        comment.content = content
        comment.save()

    @staticmethod
    def get_status_comments(status_id):
        return Comment.objects.filter(status=status_id).order_by('timestamp')

    @staticmethod
    def get_status_comments_json(status_id):
        comments = Comment.get_status_comments(status_id)
        result = []
        for comment in comments.all():
            result.append(comment.to_json())
        return result
