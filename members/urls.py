from django.urls import path
from members.views import SignUp, SignIn, profile, Friends, FriendRequestHandler, friends_view, logout, StatusHandler, \
    CommentHandler, UserInformation, delete_user

app_name = 'members'

urlpatterns = [
    path('login/', SignIn.as_view(), name='login'),
    path('signup/', SignUp.as_view(), name='signup'),
    path('info/<username>', UserInformation.as_view(), name='information'),
    path('profile/<username>', profile, name='profile'),
    path('friendsview/<username>', friends_view, name='friends_view'),
    path('friends/<username>', Friends.as_view(), name='friends'),
    path('friendrequest/<username>', FriendRequestHandler.as_view(), name='friend_requests'),
    path('friends/<username>', Friends.as_view(), name='friends'),
    path('logout', logout, name='logout'),
    path('status/', StatusHandler.as_view(), name='status'),
    path('comment/', CommentHandler.as_view(), name='comment'),
    path('delete', delete_user, name='delete')
]
