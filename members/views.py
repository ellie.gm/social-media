from django.http import HttpResponse, Http404, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import View
from .models import Member, FriendRequest, Status, Comment
from .forms import SignUpForm, SignInForm, UserInformationForm


def profile(request, username):
    try:
        member = Member.find_member_by_username(username)
        if member is not None:
            owner = is_user_owner(request, username)
            show_private = request.user.is_authenticated
            return render(request, "members/profile.html",
                          {'member': member, 'owner': owner, 'show_private': show_private, 'is_profile': True})
        else:
            return HttpResponseRedirect(reverse('error_404'))
    except Member.DoesNotExist:
        return HttpResponseRedirect(reverse('error_404'))


def delete_user(request):
    username = request.user.username
    Member.logout(request)
    Member.delete_member(username)
    return HttpResponseRedirect(reverse('members:login'))


def is_user_owner(request, username):
    return request.user.username == username


def friends_view(request, username):
    owner = is_user_owner(request, username)
    return render(request, "members/friends.html")


def logout(request):
    Member.logout(request)
    return HttpResponseRedirect(reverse('members:login'))


class Friends(View):

    def get(self, request, username):
        owner = is_user_owner(request, username)
        friends_list = {'owner': owner, 'friends': Member.get_member_friends_by_username_json(username)}
        return JsonResponse(friends_list)

    def post(self, request, username):
        owner = is_user_owner(request, username)
        action = request.POST.get('action', '')
        if action == 'view_profile':
            print('-----------------------------------------')
            return HttpResponseRedirect(reverse('members:profile', args=(request.POST.get('friend_username', ''),)))
        if action == 'delete_friend':
            Member.remove_friend(request.POST.get('username', ''), request.POST.get('friend_username', ''))
            result = {'message': 'ok'}
            return JsonResponse(result)


class FriendRequestHandler(View):
    def get(self, request, username):
        owner = is_user_owner(request, username)
        friend_request_list = {'request_list': FriendRequest.get_friend_requests_json(username)}
        return JsonResponse(friend_request_list)

    def post(self, request, username):
        action = request.POST.get('action', '')
        owner = is_user_owner(request, username)
        if action == 'delete':
            request_id = request.POST.get('request-id', '')
            FriendRequest.delete_friend_request(request_id)
            result = {'message': 'ok'}
            return JsonResponse(result)

        if action == 'create':
            sender_username = request.POST.get('sender', '')
            receiver_username = request.POST.get('receiver', '')
            FriendRequest.create_friend_request(sender_username=sender_username, receiver_username=receiver_username)
            result = {'message': 'ok'}
            return JsonResponse(result)

        if action == 'accept':
            request_id = request.POST.get('request-id', '')
            FriendRequest.accept_friend_request(request_id)
            result = {'message': 'ok'}
            return JsonResponse(result)

        if action == 'reject':
            request_id = request.POST.get('request-id', '')
            FriendRequest.reject_friend_request(request_id)
            result = {'message': 'ok'}
            return JsonResponse(result)

        if action == 'list-closed':
            sender_username = request.POST.get('sender', '')
            result = FriendRequest.get_closed_friend_requests_json(sender_username)
            result = {'data': result}
            return JsonResponse(result)


class SignUp(View):
    sign_up_form = SignUpForm
    template_name = 'members/registration_form.html'

    def get(self, request):
        form = self.sign_up_form(None)
        if request.user.is_authenticated:
            member = Member.find_member_by_username(request.user.username)
            form = self.sign_up_form(initial={'first_name': member.user.first_name, 'last_name': member.user.last_name,
                                              'username': member.user.username,
                                              'email': member.user.email, 'phone_number': member.phone_number,
                                              'address': member.address})

        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.sign_up_form(request.POST)
        if form.is_valid():
            user = form.save(commit=False)

            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            email = form.cleaned_data['email']
            phone_number = form.cleaned_data['phone_number']
            address = form.cleaned_data['address']

            member = Member()
            member.create_member(first_name=first_name, last_name=last_name, username=username, password=password,
                                 email=email, phone_number=phone_number, address=address)

            if member is not None:
                user_session = Member.login(username, password, request)

                if user_session is not None:
                    if user_session.is_active:
                        return HttpResponseRedirect(reverse('members:profile', args=(username,)))
                    else:
                        return HttpResponse("Your account is disabled.")
                else:
                    return HttpResponseRedirect(reverse('error_500'))
            else:
                return HttpResponseRedirect(reverse('error_500'))


class UserInformation(View):
    information_form = UserInformationForm
    template_name = 'members/member_info_form.html'

    def get(self, request, username):
        form = self.information_form(None)
        if request.user.is_authenticated:
            member = Member.find_member_by_username(request.user.username)
            form = self.information_form(
                initial={'first_name': member.user.first_name, 'last_name': member.user.last_name,
                         'phone_number': member.phone_number,
                         'address': member.address})

        return render(request, self.template_name, {'form': form})

    def post(self, request, username):
        form = self.information_form(request.POST)
        if form.is_valid():
            user = form.save(commit=False)

            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            phone_number = form.cleaned_data['phone_number']
            address = form.cleaned_data['address']

            if Member.update_information(username=username, first_name=first_name, last_name=last_name,
                                         phone_number=phone_number,
                                         address=address):
                return HttpResponseRedirect(reverse('members:profile', args=(username,)))
            else:
                return HttpResponseRedirect(reverse('error_500'))


class SignIn(View):
    sign_in_form = SignInForm
    template_name = 'members/login_form.html'

    def get(self, request):
        if request.user.is_authenticated:
            username = request.user.username
            return HttpResponseRedirect(reverse('members:profile', args=(username,)))
        else:
            form = self.sign_in_form(None)
            return render(request, self.template_name, {'form': form})

    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user_session = Member.login(username, password, request)

        if user_session is not None:
            if user_session.is_active:
                return HttpResponseRedirect(reverse('members:profile', args=(username,)))
            else:
                return HttpResponse("Your account is disabled.")
        else:
            form = self.sign_in_form(None)
            return render(request, self.template_name, {'form': form, 'error': 'Wrong username or password'})


class StatusHandler(View):
    def get(self, request):
        current_user = request.user.username
        status_list = Status.get_member_status_list_json(request.GET.get('username', ''), current_username=current_user)
        result = {'status_list': status_list}
        return JsonResponse(result)

    def post(self, request):
        action = request.POST.get('action')
        if action == 'create':
            content = request.POST.get('content', '')
            Status.create_status(content=content, owner_username=request.user.username)
            result = {'message': 'ok'}
            return JsonResponse(result)
        if action == 'update':
            status_id = request.POST.get('id', '')
            content = request.POST.get('content', '')
            Status.update_status(status_id=status_id, content=content)
            result = {'message': 'ok'}
            return JsonResponse(result)
        if action == 'delete':
            status_id = request.POST.get('id', '')
            Status.delete_status(status_id=status_id)
            result = {'message': 'ok'}
            return JsonResponse(result)
        return HttpResponseRedirect(reverse('error_404'))


class CommentHandler(View):

    def post(self, request):
        action = request.POST.get('action')
        if action == 'create':
            Comment.create_comment(owner_username=request.POST.get('username', ''),
                                   status_id=request.POST.get('status_id', ''), content=request.POST.get('content', ''))
            comment_list = Comment.get_status_comments_json(status_id=request.POST.get('status_id', ''))
            result = {'message': 'ok', 'comment_list': comment_list}
            return JsonResponse(result)
